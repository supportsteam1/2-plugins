<?php 
/**
 * Plugin Name: FilterBаdWords
 * Description: Фильтр плохих слов
 * Author: Rusal Vitalii
 * Version: 1.0
*/

define('MY_FILTER_DIR', plugin_dir_path(__FILE__));

function comment_filter($the_content){
	
	$badwords = [];

	if(empty($badwords)) {
		$badwords = explode(',', file_get_contents(MY_FILTER_DIR . 'bad_words.txt'));
	}

	for($i=0; $i<count($badwords); $i++) {

		$the_content = preg_replace('#'.$badwords[$i].'#iu', '*****', $the_content);
	 }

	 return $the_content;
}


add_filter( 'wp_insert_post_data', 'comment_filter' );




 ?>
